﻿FolioZipNoHDARenderer Component

*******************************************************************************
** Component Information
*******************************************************************************

	Component:              FolioZipNoHDARenderer
	Author:                 Jonathan Hult
	                        http://jonathanhult.com
	Last Updated On:        build_1_20120715

*******************************************************************************
** Overview
*******************************************************************************

	This component creates a new Content Folio rendition (zipNoHDA) which 
	creates a zip but does not add any metadata HDA files.
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	
	- 10.1.3.5.1 (111229) (Build: 7.2.4.105) 

*******************************************************************************
** HISTORY
*******************************************************************************

build_1_20120715
	- Initial component release