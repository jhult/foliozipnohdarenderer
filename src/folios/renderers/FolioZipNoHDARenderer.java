package folios.renderers;

import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataSerializeUtils;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import folios.FolioIterator;
import folios.FolioIteratorCallback;
import folios.FolioManager;
import folios.FolioRenderer;
import folios.iterators.FolioZipNoHDAIterator;

public class FolioZipNoHDARenderer implements FolioRenderer {

	public DataBinder m_folioData = null;
	public FolioIterator m_fi = null;

	public void init(final DataBinder folioData, final FolioManager manager,
			final OutputStream output) throws ServiceException {
		this.m_folioData = folioData;

		final Map options = new HashMap();

		options.put("outputStream", output);

		final String encoding = (String) manager.m_service.getLocaleResource(2);
		options.put("Encoding", DataSerializeUtils.getJavaEncoding(encoding));

		final FolioIteratorCallback  callback = new FolioZipNoHDAIterator();

		this.m_fi = new FolioIterator();
		try {
			this.m_fi.init(this.m_folioData, manager, callback, options);
		} catch (final DataException d) {
			throw new ServiceException(d);
		}
	}

	public void render() throws DataException, ServiceException {
		this.m_fi.iterate(true, null);
	}

	public String getDownloadName() {
		return this.m_folioData.getAllowMissing("RootNode") + "."
				+ getDownloadExtension();
	}

	public String getDownloadFormat() {
		return "application/zip";
	}

	public String getDownloadExtension() {
		return "zip";
	}
}