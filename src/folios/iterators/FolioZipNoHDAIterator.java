package folios.iterators;

import intradoc.common.FileUtils;
import intradoc.common.LocaleResources;
import intradoc.common.LocaleUtils;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.common.StringUtils;
import intradoc.common.SystemUtils;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.DataSerializeUtils;
import intradoc.data.ResultSet;
import intradoc.filestore.IdcFileDescriptor;
import intradoc.resource.ResourceUtils;
import intradoc.server.Service;
import intradoc.util.IdcMessage;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import folios.FolioDataHelper;
import folios.FolioIteratorCallback;
import folios.FolioManager;

public class FolioZipNoHDAIterator implements FolioIteratorCallback {
	ZipOutputStream m_zos = null;
	String m_parentDir = "";
	boolean m_hasItems = false;
	boolean m_isArchive = false;
	FolioManager m_manager = null;
	Map<String, String> m_entryFilenameMap = null;

	public void init(final DataBinder folio, final FolioManager manager,
			final Map options) throws DataException, ServiceException {
		this.m_manager = manager;
		this.m_entryFilenameMap = new HashMap<String, String>();
		final OutputStream os = (OutputStream) options.get("outputStream");
		if ((os instanceof ZipOutputStream)) {
			this.m_zos = ((ZipOutputStream) os);
		} else {
			this.m_zos = new ZipOutputStream(os);
		}
		this.m_isArchive = StringUtils.convertToBool(
				(String) options.get("isZipArchive"), false);
	}

	public void finish() throws DataException, ServiceException {
		try {
			this.m_zos.close();
		} catch (final IOException ioe) {
		}
	}

	public void beginNode(final int depth, final String id, final String type,
			final DataBinder folio) throws DataException, ServiceException {
		@SuppressWarnings("unchecked")
		final Map<String, String> props = FolioDataHelper.getNodeProperties(
				folio, id);
		final String dFormat = props.get("xcsd:dFormat");
		if ((type.equals("node"))
				|| ((dFormat != null) && (dFormat.equals("text/xcsr")))) {
			final String name = LocaleResources.localizeMessage(
					LocaleUtils.encodeMessage(props.get("xcst:name"), null),
					this.m_manager.m_service);

			this.m_parentDir = (this.m_parentDir + name + "/");
		} else {
			final String docName = props.get("xcsd:dDocName");
			if ((docName == null) || (docName.length() == 0)) {
				return;
			}

			if (StringUtils
					.convertToBool(props.get("xcst:isLinkBroken"), false)) {
				return;
			}

			String query = null;
			final DataBinder serviceBinder = new DataBinder();
			final boolean isLocked = StringUtils.convertToBool(
					props.get("xcst:lockContentID"), false);

			final boolean isArchive = StringUtils.convertToBool(
					FolioDataHelper.getRootProperty(folio, "xcst:isArchive"),
					false);

			if ((isLocked) || (isArchive)) {
				final String dID = props.get("xcsd:dID");
				serviceBinder.putLocal("dID", dID);
				query = "QdocInfo";
			} else {
				serviceBinder.putLocal("dDocName", docName);

				query = "QdocInfoCurrentIndexed";
			}

			final ResultSet rset = this.m_manager.m_workspace.createResultSet(
					query, serviceBinder);
			if (rset.isEmpty()) {
				throw new ServiceException(null, new IdcMessage(
						"csCpdCouldNotFindItemToArchive",
						new Object[] { docName }));
			}

			final DataResultSet drset = new DataResultSet();
			drset.copy(rset);
			serviceBinder.addResultSet("DOC_INFO", drset);

			final Properties metaProps = drset.getCurrentRowProps();
			final DataBinder metaBinder = new DataBinder();
			if (metaProps != null) {
				metaBinder.setLocalData(metaProps);
			}

			Report.trace("folios",
					"Write meta file. " + metaBinder.m_localData, null);
			final String tmpMetaFileName = DataBinder.getNextFileCounter()
					+ ".hda";
			ResourceUtils.serializeDataBinder(
					DataBinder.getTemporaryDirectory(), tmpMetaFileName,
					metaBinder, true, false);

			Report.trace("folios", "Done writing meta file.", null);

			final Service service = this.m_manager.m_service;

			final IdcFileDescriptor d = service.m_fileUtils
					.createDescriptorForRendition(serviceBinder,
							"webViewableFile");

			this.m_hasItems = true;

			final String extension = serviceBinder.get("dWebExtension");
			//Object[][] fileList = new Object[2][2];
			final Object[][] fileList = new Object[1][2];
			if (this.m_isArchive) {
				fileList[0] = new Object[] { d, id + "/file." + extension };
				// fileList[1] = { DataBinder.getTemporaryDirectory() +
				// tmpMetaFileName, id + "/metadata.hda" };
			} else {
				final String displayName = LocaleResources.getString(
						props.get("xcst:name"), service);

				final String encoding = DataSerializeUtils.determineEncoding(
						service.getBinder(), service);

				String pathRoot = this.m_parentDir + displayName;
				if (this.m_entryFilenameMap.get(pathRoot) != null) {
					int count = 2;
					String newPathRoot = pathRoot;

					while (this.m_entryFilenameMap.get(newPathRoot) != null) {
						newPathRoot = pathRoot + " (" + count++ + ")";
					}

					pathRoot = newPathRoot;
				}
				this.m_entryFilenameMap.put(pathRoot, pathRoot);

				pathRoot = StringUtils.urlEscape7Bit(pathRoot, '%', encoding);

				fileList[0] = new Object[] { d, pathRoot + "." + extension };
				// fileList[1] = { DataBinder.getTemporaryDirectory() +
				// tmpMetaFileName, pathRoot + "_metadata.hda" };
			}

			InputStream in = null;
			for (int i = 0; i < fileList.length; i++) {
				try {
					final Object fileObj = fileList[i];
					if ((fileObj instanceof String)) {
						in = new FileInputStream((String) fileObj);
					} else {
						in = service.m_fileStore.getInputStream(d, null);
					}
					final ZipEntry entry = new ZipEntry((String)fileList[i][1]);
					this.m_zos.putNextEntry(entry);

					final byte[] buf = new byte[16384];
					int num = 0;
					while ((num = in.read(buf)) > 0) {
						this.m_zos.write(buf, 0, num);
					}

				} catch (final Exception e) {
					throw new DataException("!csCpdCouldNotWriteZipFile", e);
				} finally {
					FileUtils.closeObject(in);
					try {
						this.m_zos.closeEntry();
					} catch (final Exception e) {
						SystemUtils.traceDumpException("folios",
								"Error closing zip stream.", e);
					}
				}
			}

			@SuppressWarnings("unchecked")
			ArrayList<String> currentTempFiles = (ArrayList<String>) service
			.getCachedObject("FolioTempFiles");
			if (currentTempFiles == null) {
				currentTempFiles = new ArrayList<String>();
			}

			currentTempFiles.add(DataBinder.getTemporaryDirectory()
					+ tmpMetaFileName);
			service.setCachedObject("FolioTempFiles", currentTempFiles);
		}
	}

	public void endNode(final int depth, final String id, final String type,
			final DataBinder folio) throws DataException, ServiceException {
		@SuppressWarnings("unchecked")
		final Map<String, String> props = FolioDataHelper.getNodeProperties(
				folio, id);
		final String dFormat = props.get("xcsd:dFormat");
		if ((type.equals("node"))
				|| ((dFormat != null) && (dFormat.equals("text/xcsr")))) {
			final int index = this.m_parentDir.lastIndexOf('/',
					this.m_parentDir.length() - 2);
			if (index > 0) {
				this.m_parentDir = this.m_parentDir.substring(0, index + 1);
			} else {
				this.m_parentDir = "";
			}
		}
	}
}
